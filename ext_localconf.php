<?php

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') || die();

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'SisBase',
        'Reports',
        [
            \T3graf\SisBase\Controller\ClubsController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\TeamsController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\AgeGroupsController::class => 'new, create, edit, update, delete',
            \T3graf\SisBase\Controller\LocationController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\ProfilesController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\CompetitionsController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\SeasonController::class => 'list, show, new, create, edit, update',
            \T3graf\SisBase\Controller\MatchesController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\EventController::class => 'list, show, new, create',
        ],
        // non-cacheable actions
        [
            \T3graf\SisBase\Controller\ClubsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\TeamsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\AgeGroupsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\LocationController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\ProfilesController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\CompetitionsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\SeasonController::class => 'create, update',
            \T3graf\SisBase\Controller\MatchesController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\EventController::class => 'create',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'SisBase',
        'Matches',
        [
            \T3graf\SisBase\Controller\ClubsController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\TeamsController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\AgeGroupsController::class => 'new, create, edit, update, delete',
            \T3graf\SisBase\Controller\LocationController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\ProfilesController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\CompetitionsController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\SeasonController::class => 'list, show, new, create, edit, update',
            \T3graf\SisBase\Controller\MatchesController::class => 'list, show, new, create, edit, update, delete',
            \T3graf\SisBase\Controller\EventController::class => 'list, show, new, create',
        ],
        // non-cacheable actions
        [
            \T3graf\SisBase\Controller\ClubsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\TeamsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\AgeGroupsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\LocationController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\ProfilesController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\CompetitionsController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\SeasonController::class => 'create, update',
            \T3graf\SisBase\Controller\MatchesController::class => 'create, update, delete',
            \T3graf\SisBase\Controller\EventController::class => 'create',
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    reports {
                        iconIdentifier = sis_base-plugin-reports
                        title = LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sis_base_reports.name
                        description = LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sis_base_reports.description
                        tt_content_defValues {
                            CType = list
                            list_type = sisbase_reports
                        }
                    }
                    matches {
                        iconIdentifier = sis_base-plugin-matches
                        title = LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sis_base_matches.name
                        description = LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sis_base_matches.description
                        tt_content_defValues {
                            CType = list
                            list_type = sisbase_matches
                        }
                    }
                }
                show = *
            }
       }'
    );
})();
