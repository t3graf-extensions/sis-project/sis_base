<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class SeasonTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Season|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Season::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName(): void
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('name'));
    }

    /**
     * @test
     */
    public function getWinterBreakReturnsInitialValueForDateTime(): void
    {
        self::assertNull(
            $this->subject->getWinterBreak()
        );
    }

    /**
     * @test
     */
    public function setWinterBreakForDateTimeSetsWinterBreak(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setWinterBreak($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('winterBreak'));
    }
}
