<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class ProfilesTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Profiles|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Profiles::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getFirstNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getFirstName()
        );
    }

    /**
     * @test
     */
    public function setFirstNameForStringSetsFirstName(): void
    {
        $this->subject->setFirstName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('firstName'));
    }

    /**
     * @test
     */
    public function getLastNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getLastName()
        );
    }

    /**
     * @test
     */
    public function setLastNameForStringSetsLastName(): void
    {
        $this->subject->setLastName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('lastName'));
    }

    /**
     * @test
     */
    public function getStageNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getStageName()
        );
    }

    /**
     * @test
     */
    public function setStageNameForStringSetsStageName(): void
    {
        $this->subject->setStageName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('stageName'));
    }

    /**
     * @test
     */
    public function getProfileImagesReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getProfileImages()
        );
    }

    /**
     * @test
     */
    public function setProfileImagesForFileReferenceSetsProfileImages(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setProfileImages($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('profileImages'));
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity(): void
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('city'));
    }

    /**
     * @test
     */
    public function getBirthdayReturnsInitialValueForDateTime(): void
    {
        self::assertNull(
            $this->subject->getBirthday()
        );
    }

    /**
     * @test
     */
    public function setBirthdayForDateTimeSetsBirthday(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setBirthday($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('birthday'));
    }

    /**
     * @test
     */
    public function getBirthplaceReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getBirthplace()
        );
    }

    /**
     * @test
     */
    public function setBirthplaceForStringSetsBirthplace(): void
    {
        $this->subject->setBirthplace('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('birthplace'));
    }

    /**
     * @test
     */
    public function getNationalityReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getNationality()
        );
    }

    /**
     * @test
     */
    public function setNationalityForStringSetsNationality(): void
    {
        $this->subject->setNationality('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('nationality'));
    }

    /**
     * @test
     */
    public function getGenderReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getGender()
        );
    }

    /**
     * @test
     */
    public function setGenderForIntSetsGender(): void
    {
        $this->subject->setGender(12);

        self::assertEquals(12, $this->subject->_get('gender'));
    }

    /**
     * @test
     */
    public function getHeightReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getHeight()
        );
    }

    /**
     * @test
     */
    public function setHeightForStringSetsHeight(): void
    {
        $this->subject->setHeight('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('height'));
    }

    /**
     * @test
     */
    public function getWeightReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getWeight()
        );
    }

    /**
     * @test
     */
    public function setWeightForStringSetsWeight(): void
    {
        $this->subject->setWeight('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('weight'));
    }

    /**
     * @test
     */
    public function getContractStartReturnsInitialValueForDateTime(): void
    {
        self::assertNull(
            $this->subject->getContractStart()
        );
    }

    /**
     * @test
     */
    public function setContractStartForDateTimeSetsContractStart(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setContractStart($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('contractStart'));
    }

    /**
     * @test
     */
    public function getContractEndReturnsInitialValueForDateTime(): void
    {
        self::assertNull(
            $this->subject->getContractEnd()
        );
    }

    /**
     * @test
     */
    public function setContractEndForDateTimeSetsContractEnd(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setContractEnd($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('contractEnd'));
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail(): void
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('email'));
    }

    /**
     * @test
     */
    public function getNicknameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getNickname()
        );
    }

    /**
     * @test
     */
    public function setNicknameForStringSetsNickname(): void
    {
        $this->subject->setNickname('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('nickname'));
    }

    /**
     * @test
     */
    public function getGdprReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getGdpr()
        );
    }

    /**
     * @test
     */
    public function setGdprForIntSetsGdpr(): void
    {
        $this->subject->setGdpr(12);

        self::assertEquals(12, $this->subject->_get('gdpr'));
    }

    /**
     * @test
     */
    public function getImagesReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getImages()
        );
    }

    /**
     * @test
     */
    public function setImagesForFileReferenceSetsImages(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImages($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('images'));
    }

    /**
     * @test
     */
    public function getLastClubsReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getLastClubs()
        );
    }

    /**
     * @test
     */
    public function setLastClubsForStringSetsLastClubs(): void
    {
        $this->subject->setLastClubs('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('lastClubs'));
    }

    /**
     * @test
     */
    public function getShortDescriptionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getShortDescription()
        );
    }

    /**
     * @test
     */
    public function setShortDescriptionForStringSetsShortDescription(): void
    {
        $this->subject->setShortDescription('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('shortDescription'));
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription(): void
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('description'));
    }

    /**
     * @test
     */
    public function getProfileTypReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getProfileTyp()
        );
    }

    /**
     * @test
     */
    public function setProfileTypForIntSetsProfileTyp(): void
    {
        $this->subject->setProfileTyp(12);

        self::assertEquals(12, $this->subject->_get('profileTyp'));
    }

    /**
     * @test
     */
    public function getJerseyNumberReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getJerseyNumber()
        );
    }

    /**
     * @test
     */
    public function setJerseyNumberForIntSetsJerseyNumber(): void
    {
        $this->subject->setJerseyNumber(12);

        self::assertEquals(12, $this->subject->_get('jerseyNumber'));
    }

    /**
     * @test
     */
    public function getPositionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getPosition()
        );
    }

    /**
     * @test
     */
    public function setPositionForStringSetsPosition(): void
    {
        $this->subject->setPosition('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('position'));
    }
}
