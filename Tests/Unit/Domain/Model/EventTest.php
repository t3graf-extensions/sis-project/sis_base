<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class EventTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Event|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Event::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName(): void
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('name'));
    }

    /**
     * @test
     */
    public function getCompetitionsReturnsInitialValueForCompetitions(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getCompetitions()
        );
    }

    /**
     * @test
     */
    public function setCompetitionsForObjectStorageContainingCompetitionsSetsCompetitions(): void
    {
        $competition = new \T3graf\SisBase\Domain\Model\Competitions();
        $objectStorageHoldingExactlyOneCompetitions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneCompetitions->attach($competition);
        $this->subject->setCompetitions($objectStorageHoldingExactlyOneCompetitions);

        self::assertEquals($objectStorageHoldingExactlyOneCompetitions, $this->subject->_get('competitions'));
    }

    /**
     * @test
     */
    public function addCompetitionToObjectStorageHoldingCompetitions(): void
    {
        $competition = new \T3graf\SisBase\Domain\Model\Competitions();
        $competitionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $competitionsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($competition));
        $this->subject->_set('competitions', $competitionsObjectStorageMock);

        $this->subject->addCompetition($competition);
    }

    /**
     * @test
     */
    public function removeCompetitionFromObjectStorageHoldingCompetitions(): void
    {
        $competition = new \T3graf\SisBase\Domain\Model\Competitions();
        $competitionsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $competitionsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($competition));
        $this->subject->_set('competitions', $competitionsObjectStorageMock);

        $this->subject->removeCompetition($competition);
    }
}
