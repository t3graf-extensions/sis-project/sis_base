<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class TeamsTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Teams|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Teams::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName(): void
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('name'));
    }

    /**
     * @test
     */
    public function getShortNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getShortName()
        );
    }

    /**
     * @test
     */
    public function setShortNameForStringSetsShortName(): void
    {
        $this->subject->setShortName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('shortName'));
    }

    /**
     * @test
     */
    public function getLogoReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getLogo()
        );
    }

    /**
     * @test
     */
    public function setLogoForFileReferenceSetsLogo(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLogo($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('logo'));
    }

    /**
     * @test
     */
    public function getImagesReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getImages()
        );
    }

    /**
     * @test
     */
    public function setImagesForFileReferenceSetsImages(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImages($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('images'));
    }

    /**
     * @test
     */
    public function getTeamCommentReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getTeamComment()
        );
    }

    /**
     * @test
     */
    public function setTeamCommentForStringSetsTeamComment(): void
    {
        $this->subject->setTeamComment('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('teamComment'));
    }

    /**
     * @test
     */
    public function getPlayersCommentReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getPlayersComment()
        );
    }

    /**
     * @test
     */
    public function setPlayersCommentForStringSetsPlayersComment(): void
    {
        $this->subject->setPlayersComment('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('playersComment'));
    }

    /**
     * @test
     */
    public function getCoachesCommentReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCoachesComment()
        );
    }

    /**
     * @test
     */
    public function setCoachesCommentForStringSetsCoachesComment(): void
    {
        $this->subject->setCoachesComment('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('coachesComment'));
    }

    /**
     * @test
     */
    public function getSupervisorsCommentReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getSupervisorsComment()
        );
    }

    /**
     * @test
     */
    public function setSupervisorsCommentForStringSetsSupervisorsComment(): void
    {
        $this->subject->setSupervisorsComment('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('supervisorsComment'));
    }

    /**
     * @test
     */
    public function getClubReturnsInitialValueForClubs(): void
    {
        self::assertNull(
            $this->subject->getClub()
        );
    }

    /**
     * @test
     */
    public function setClubForClubsSetsClub(): void
    {
        $clubFixture = new \T3graf\SisBase\Domain\Model\Clubs();
        $this->subject->setClub($clubFixture);

        self::assertEquals($clubFixture, $this->subject->_get('club'));
    }

    /**
     * @test
     */
    public function getAgeGroupReturnsInitialValueForAgeGroups(): void
    {
        self::assertNull(
            $this->subject->getAgeGroup()
        );
    }

    /**
     * @test
     */
    public function setAgeGroupForAgeGroupsSetsAgeGroup(): void
    {
        $ageGroupFixture = new \T3graf\SisBase\Domain\Model\AgeGroups();
        $this->subject->setAgeGroup($ageGroupFixture);

        self::assertEquals($ageGroupFixture, $this->subject->_get('ageGroup'));
    }

    /**
     * @test
     */
    public function getCoachesReturnsInitialValueForProfiles(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getCoaches()
        );
    }

    /**
     * @test
     */
    public function setCoachesForObjectStorageContainingProfilesSetsCoaches(): void
    {
        $coach = new \T3graf\SisBase\Domain\Model\Profiles();
        $objectStorageHoldingExactlyOneCoaches = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneCoaches->attach($coach);
        $this->subject->setCoaches($objectStorageHoldingExactlyOneCoaches);

        self::assertEquals($objectStorageHoldingExactlyOneCoaches, $this->subject->_get('coaches'));
    }

    /**
     * @test
     */
    public function addCoachToObjectStorageHoldingCoaches(): void
    {
        $coach = new \T3graf\SisBase\Domain\Model\Profiles();
        $coachesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $coachesObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($coach));
        $this->subject->_set('coaches', $coachesObjectStorageMock);

        $this->subject->addCoach($coach);
    }

    /**
     * @test
     */
    public function removeCoachFromObjectStorageHoldingCoaches(): void
    {
        $coach = new \T3graf\SisBase\Domain\Model\Profiles();
        $coachesObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $coachesObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($coach));
        $this->subject->_set('coaches', $coachesObjectStorageMock);

        $this->subject->removeCoach($coach);
    }

    /**
     * @test
     */
    public function getPlayersReturnsInitialValueForProfiles(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPlayers()
        );
    }

    /**
     * @test
     */
    public function setPlayersForObjectStorageContainingProfilesSetsPlayers(): void
    {
        $player = new \T3graf\SisBase\Domain\Model\Profiles();
        $objectStorageHoldingExactlyOnePlayers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePlayers->attach($player);
        $this->subject->setPlayers($objectStorageHoldingExactlyOnePlayers);

        self::assertEquals($objectStorageHoldingExactlyOnePlayers, $this->subject->_get('players'));
    }

    /**
     * @test
     */
    public function addPlayerToObjectStorageHoldingPlayers(): void
    {
        $player = new \T3graf\SisBase\Domain\Model\Profiles();
        $playersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $playersObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($player));
        $this->subject->_set('players', $playersObjectStorageMock);

        $this->subject->addPlayer($player);
    }

    /**
     * @test
     */
    public function removePlayerFromObjectStorageHoldingPlayers(): void
    {
        $player = new \T3graf\SisBase\Domain\Model\Profiles();
        $playersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $playersObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($player));
        $this->subject->_set('players', $playersObjectStorageMock);

        $this->subject->removePlayer($player);
    }

    /**
     * @test
     */
    public function getSupervisorsReturnsInitialValueForProfiles(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getSupervisors()
        );
    }

    /**
     * @test
     */
    public function setSupervisorsForObjectStorageContainingProfilesSetsSupervisors(): void
    {
        $supervisor = new \T3graf\SisBase\Domain\Model\Profiles();
        $objectStorageHoldingExactlyOneSupervisors = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneSupervisors->attach($supervisor);
        $this->subject->setSupervisors($objectStorageHoldingExactlyOneSupervisors);

        self::assertEquals($objectStorageHoldingExactlyOneSupervisors, $this->subject->_get('supervisors'));
    }

    /**
     * @test
     */
    public function addSupervisorToObjectStorageHoldingSupervisors(): void
    {
        $supervisor = new \T3graf\SisBase\Domain\Model\Profiles();
        $supervisorsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $supervisorsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($supervisor));
        $this->subject->_set('supervisors', $supervisorsObjectStorageMock);

        $this->subject->addSupervisor($supervisor);
    }

    /**
     * @test
     */
    public function removeSupervisorFromObjectStorageHoldingSupervisors(): void
    {
        $supervisor = new \T3graf\SisBase\Domain\Model\Profiles();
        $supervisorsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $supervisorsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($supervisor));
        $this->subject->_set('supervisors', $supervisorsObjectStorageMock);

        $this->subject->removeSupervisor($supervisor);
    }
}
