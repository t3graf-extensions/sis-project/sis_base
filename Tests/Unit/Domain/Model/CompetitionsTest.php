<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class CompetitionsTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Competitions|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Competitions::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName(): void
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('name'));
    }

    /**
     * @test
     */
    public function getShortNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getShortName()
        );
    }

    /**
     * @test
     */
    public function setShortNameForStringSetsShortName(): void
    {
        $this->subject->setShortName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('shortName'));
    }

    /**
     * @test
     */
    public function getLogoReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getLogo()
        );
    }

    /**
     * @test
     */
    public function setLogoForFileReferenceSetsLogo(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLogo($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('logo'));
    }

    /**
     * @test
     */
    public function getSportsReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getSports()
        );
    }

    /**
     * @test
     */
    public function setSportsForIntSetsSports(): void
    {
        $this->subject->setSports(12);

        self::assertEquals(12, $this->subject->_get('sports'));
    }

    /**
     * @test
     */
    public function getCompetitionModeReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getCompetitionMode()
        );
    }

    /**
     * @test
     */
    public function setCompetitionModeForIntSetsCompetitionMode(): void
    {
        $this->subject->setCompetitionMode(12);

        self::assertEquals(12, $this->subject->_get('competitionMode'));
    }

    /**
     * @test
     */
    public function getCompetitionTypReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getCompetitionTyp()
        );
    }

    /**
     * @test
     */
    public function setCompetitionTypForIntSetsCompetitionTyp(): void
    {
        $this->subject->setCompetitionTyp(12);

        self::assertEquals(12, $this->subject->_get('competitionTyp'));
    }

    /**
     * @test
     */
    public function getAgeGroupsReturnsInitialValueForAgeGroups(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getAgeGroups()
        );
    }

    /**
     * @test
     */
    public function setAgeGroupsForObjectStorageContainingAgeGroupsSetsAgeGroups(): void
    {
        $ageGroup = new \T3graf\SisBase\Domain\Model\AgeGroups();
        $objectStorageHoldingExactlyOneAgeGroups = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneAgeGroups->attach($ageGroup);
        $this->subject->setAgeGroups($objectStorageHoldingExactlyOneAgeGroups);

        self::assertEquals($objectStorageHoldingExactlyOneAgeGroups, $this->subject->_get('ageGroups'));
    }

    /**
     * @test
     */
    public function addAgeGroupToObjectStorageHoldingAgeGroups(): void
    {
        $ageGroup = new \T3graf\SisBase\Domain\Model\AgeGroups();
        $ageGroupsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $ageGroupsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($ageGroup));
        $this->subject->_set('ageGroups', $ageGroupsObjectStorageMock);

        $this->subject->addAgeGroup($ageGroup);
    }

    /**
     * @test
     */
    public function removeAgeGroupFromObjectStorageHoldingAgeGroups(): void
    {
        $ageGroup = new \T3graf\SisBase\Domain\Model\AgeGroups();
        $ageGroupsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $ageGroupsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($ageGroup));
        $this->subject->_set('ageGroups', $ageGroupsObjectStorageMock);

        $this->subject->removeAgeGroup($ageGroup);
    }

    /**
     * @test
     */
    public function getSeasonReturnsInitialValueForSeason(): void
    {
        self::assertNull(
            $this->subject->getSeason()
        );
    }

    /**
     * @test
     */
    public function setSeasonForSeasonSetsSeason(): void
    {
        $seasonFixture = new \T3graf\SisBase\Domain\Model\Season();
        $this->subject->setSeason($seasonFixture);

        self::assertEquals($seasonFixture, $this->subject->_get('season'));
    }

    /**
     * @test
     */
    public function getTeamsReturnsInitialValueForTeams(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getTeams()
        );
    }

    /**
     * @test
     */
    public function setTeamsForObjectStorageContainingTeamsSetsTeams(): void
    {
        $team = new \T3graf\SisBase\Domain\Model\Teams();
        $objectStorageHoldingExactlyOneTeams = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneTeams->attach($team);
        $this->subject->setTeams($objectStorageHoldingExactlyOneTeams);

        self::assertEquals($objectStorageHoldingExactlyOneTeams, $this->subject->_get('teams'));
    }

    /**
     * @test
     */
    public function addTeamToObjectStorageHoldingTeams(): void
    {
        $team = new \T3graf\SisBase\Domain\Model\Teams();
        $teamsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $teamsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($team));
        $this->subject->_set('teams', $teamsObjectStorageMock);

        $this->subject->addTeam($team);
    }

    /**
     * @test
     */
    public function removeTeamFromObjectStorageHoldingTeams(): void
    {
        $team = new \T3graf\SisBase\Domain\Model\Teams();
        $teamsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $teamsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($team));
        $this->subject->_set('teams', $teamsObjectStorageMock);

        $this->subject->removeTeam($team);
    }

    /**
     * @test
     */
    public function getPlayersReturnsInitialValueForProfiles(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getPlayers()
        );
    }

    /**
     * @test
     */
    public function setPlayersForObjectStorageContainingProfilesSetsPlayers(): void
    {
        $player = new \T3graf\SisBase\Domain\Model\Profiles();
        $objectStorageHoldingExactlyOnePlayers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOnePlayers->attach($player);
        $this->subject->setPlayers($objectStorageHoldingExactlyOnePlayers);

        self::assertEquals($objectStorageHoldingExactlyOnePlayers, $this->subject->_get('players'));
    }

    /**
     * @test
     */
    public function addPlayerToObjectStorageHoldingPlayers(): void
    {
        $player = new \T3graf\SisBase\Domain\Model\Profiles();
        $playersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $playersObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($player));
        $this->subject->_set('players', $playersObjectStorageMock);

        $this->subject->addPlayer($player);
    }

    /**
     * @test
     */
    public function removePlayerFromObjectStorageHoldingPlayers(): void
    {
        $player = new \T3graf\SisBase\Domain\Model\Profiles();
        $playersObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $playersObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($player));
        $this->subject->_set('players', $playersObjectStorageMock);

        $this->subject->removePlayer($player);
    }
}
