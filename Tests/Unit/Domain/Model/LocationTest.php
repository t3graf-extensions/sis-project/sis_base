<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class LocationTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Location|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Location::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName(): void
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('name'));
    }

    /**
     * @test
     */
    public function getAltNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAltName()
        );
    }

    /**
     * @test
     */
    public function setAltNameForStringSetsAltName(): void
    {
        $this->subject->setAltName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('altName'));
    }

    /**
     * @test
     */
    public function getCapacityReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getCapacity()
        );
    }

    /**
     * @test
     */
    public function setCapacityForIntSetsCapacity(): void
    {
        $this->subject->setCapacity(12);

        self::assertEquals(12, $this->subject->_get('capacity'));
    }

    /**
     * @test
     */
    public function getLogoReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getLogo()
        );
    }

    /**
     * @test
     */
    public function setLogoForFileReferenceSetsLogo(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLogo($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('logo'));
    }

    /**
     * @test
     */
    public function getImagesReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getImages()
        );
    }

    /**
     * @test
     */
    public function setImagesForFileReferenceSetsImages(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImages($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('images'));
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription(): void
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('description'));
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet(): void
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('street'));
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity(): void
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('city'));
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForStringSetsZip(): void
    {
        $this->subject->setZip('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('zip'));
    }

    /**
     * @test
     */
    public function getCountryReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCountry()
        );
    }

    /**
     * @test
     */
    public function setCountryForStringSetsCountry(): void
    {
        $this->subject->setCountry('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('country'));
    }

    /**
     * @test
     */
    public function getCountryCodeReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCountryCode()
        );
    }

    /**
     * @test
     */
    public function setCountryCodeForStringSetsCountryCode(): void
    {
        $this->subject->setCountryCode('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('countryCode'));
    }

    /**
     * @test
     */
    public function getLngReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getLng()
        );
    }

    /**
     * @test
     */
    public function setLngForIntSetsLng(): void
    {
        $this->subject->setLng(12);

        self::assertEquals(12, $this->subject->_get('lng'));
    }

    /**
     * @test
     */
    public function getLatReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getLat()
        );
    }

    /**
     * @test
     */
    public function setLatForIntSetsLat(): void
    {
        $this->subject->setLat(12);

        self::assertEquals(12, $this->subject->_get('lat'));
    }
}
