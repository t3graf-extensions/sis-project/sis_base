<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class MatchesTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Matches|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Matches::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getMatchNoReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getMatchNo()
        );
    }

    /**
     * @test
     */
    public function setMatchNoForStringSetsMatchNo(): void
    {
        $this->subject->setMatchNo('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('matchNo'));
    }

    /**
     * @test
     */
    public function getRoundReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getRound()
        );
    }

    /**
     * @test
     */
    public function setRoundForIntSetsRound(): void
    {
        $this->subject->setRound(12);

        self::assertEquals(12, $this->subject->_get('round'));
    }

    /**
     * @test
     */
    public function getRoundNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getRoundName()
        );
    }

    /**
     * @test
     */
    public function setRoundNameForStringSetsRoundName(): void
    {
        $this->subject->setRoundName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('roundName'));
    }

    /**
     * @test
     */
    public function getStatusReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getStatus()
        );
    }

    /**
     * @test
     */
    public function setStatusForIntSetsStatus(): void
    {
        $this->subject->setStatus(12);

        self::assertEquals(12, $this->subject->_get('status'));
    }

    /**
     * @test
     */
    public function getAddInfoReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getAddInfo()
        );
    }

    /**
     * @test
     */
    public function setAddInfoForStringSetsAddInfo(): void
    {
        $this->subject->setAddInfo('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('addInfo'));
    }

    /**
     * @test
     */
    public function getDateReturnsInitialValueForDateTime(): void
    {
        self::assertNull(
            $this->subject->getDate()
        );
    }

    /**
     * @test
     */
    public function setDateForDateTimeSetsDate(): void
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate($dateTimeFixture);

        self::assertEquals($dateTimeFixture, $this->subject->_get('date'));
    }

    /**
     * @test
     */
    public function getVisitorsReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getVisitors()
        );
    }

    /**
     * @test
     */
    public function setVisitorsForIntSetsVisitors(): void
    {
        $this->subject->setVisitors(12);

        self::assertEquals(12, $this->subject->_get('visitors'));
    }

    /**
     * @test
     */
    public function getMatchReportReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getMatchReport()
        );
    }

    /**
     * @test
     */
    public function setMatchReportForStringSetsMatchReport(): void
    {
        $this->subject->setMatchReport('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('matchReport'));
    }

    /**
     * @test
     */
    public function getReportAuthorReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getReportAuthor()
        );
    }

    /**
     * @test
     */
    public function setReportAuthorForStringSetsReportAuthor(): void
    {
        $this->subject->setReportAuthor('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('reportAuthor'));
    }

    /**
     * @test
     */
    public function getReportLinkReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getReportLink()
        );
    }

    /**
     * @test
     */
    public function setReportLinkForStringSetsReportLink(): void
    {
        $this->subject->setReportLink('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('reportLink'));
    }

    /**
     * @test
     */
    public function getImagesReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getImages()
        );
    }

    /**
     * @test
     */
    public function setImagesForFileReferenceSetsImages(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImages($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('images'));
    }

    /**
     * @test
     */
    public function getLocationReturnsInitialValueForLocation(): void
    {
        self::assertNull(
            $this->subject->getLocation()
        );
    }

    /**
     * @test
     */
    public function setLocationForLocationSetsLocation(): void
    {
        $locationFixture = new \T3graf\SisBase\Domain\Model\Location();
        $this->subject->setLocation($locationFixture);

        self::assertEquals($locationFixture, $this->subject->_get('location'));
    }

    /**
     * @test
     */
    public function getCompetitionReturnsInitialValueForCompetitions(): void
    {
        self::assertNull(
            $this->subject->getCompetition()
        );
    }

    /**
     * @test
     */
    public function setCompetitionForCompetitionsSetsCompetition(): void
    {
        $competitionFixture = new \T3graf\SisBase\Domain\Model\Competitions();
        $this->subject->setCompetition($competitionFixture);

        self::assertEquals($competitionFixture, $this->subject->_get('competition'));
    }
}
