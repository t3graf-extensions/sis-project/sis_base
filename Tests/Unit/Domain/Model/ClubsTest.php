<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Tests\Unit\Domain\Model;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Test case
 */
class ClubsTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Domain\Model\Clubs|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();

        $this->subject = $this->getAccessibleMock(
            \T3graf\SisBase\Domain\Model\Clubs::class,
            ['dummy']
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName(): void
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('name'));
    }

    /**
     * @test
     */
    public function getShortNameReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getShortName()
        );
    }

    /**
     * @test
     */
    public function setShortNameForStringSetsShortName(): void
    {
        $this->subject->setShortName('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('shortName'));
    }

    /**
     * @test
     */
    public function getLogoReturnsInitialValueForFileReference(): void
    {
        self::assertNull(
            $this->subject->getLogo()
        );
    }

    /**
     * @test
     */
    public function setLogoForFileReferenceSetsLogo(): void
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setLogo($fileReferenceFixture);

        self::assertEquals($fileReferenceFixture, $this->subject->_get('logo'));
    }

    /**
     * @test
     */
    public function getStreetReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getStreet()
        );
    }

    /**
     * @test
     */
    public function setStreetForStringSetsStreet(): void
    {
        $this->subject->setStreet('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('street'));
    }

    /**
     * @test
     */
    public function getZipReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getZip()
        );
    }

    /**
     * @test
     */
    public function setZipForStringSetsZip(): void
    {
        $this->subject->setZip('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('zip'));
    }

    /**
     * @test
     */
    public function getCityReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCity()
        );
    }

    /**
     * @test
     */
    public function setCityForStringSetsCity(): void
    {
        $this->subject->setCity('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('city'));
    }

    /**
     * @test
     */
    public function getCountryReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCountry()
        );
    }

    /**
     * @test
     */
    public function setCountryForStringSetsCountry(): void
    {
        $this->subject->setCountry('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('country'));
    }

    /**
     * @test
     */
    public function getCountryCodeReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getCountryCode()
        );
    }

    /**
     * @test
     */
    public function setCountryCodeForStringSetsCountryCode(): void
    {
        $this->subject->setCountryCode('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('countryCode'));
    }

    /**
     * @test
     */
    public function getLngReturnsInitialValueForFloat(): void
    {
        self::assertSame(
            0.0,
            $this->subject->getLng()
        );
    }

    /**
     * @test
     */
    public function setLngForFloatSetsLng(): void
    {
        $this->subject->setLng(3.14159265);

        self::assertEquals(3.14159265, $this->subject->_get('lng'));
    }

    /**
     * @test
     */
    public function getLatReturnsInitialValueForFloat(): void
    {
        self::assertSame(
            0.0,
            $this->subject->getLat()
        );
    }

    /**
     * @test
     */
    public function setLatForFloatSetsLat(): void
    {
        $this->subject->setLat(3.14159265);

        self::assertEquals(3.14159265, $this->subject->_get('lat'));
    }

    /**
     * @test
     */
    public function getShortInfoReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getShortInfo()
        );
    }

    /**
     * @test
     */
    public function setShortInfoForStringSetsShortInfo(): void
    {
        $this->subject->setShortInfo('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('shortInfo'));
    }

    /**
     * @test
     */
    public function getDescriptionReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getDescription()
        );
    }

    /**
     * @test
     */
    public function setDescriptionForStringSetsDescription(): void
    {
        $this->subject->setDescription('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('description'));
    }

    /**
     * @test
     */
    public function getFoundingYearReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getFoundingYear()
        );
    }

    /**
     * @test
     */
    public function setFoundingYearForIntSetsFoundingYear(): void
    {
        $this->subject->setFoundingYear(12);

        self::assertEquals(12, $this->subject->_get('foundingYear'));
    }

    /**
     * @test
     */
    public function getClubColorsReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getClubColors()
        );
    }

    /**
     * @test
     */
    public function setClubColorsForStringSetsClubColors(): void
    {
        $this->subject->setClubColors('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('clubColors'));
    }

    /**
     * @test
     */
    public function getWwwReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getWww()
        );
    }

    /**
     * @test
     */
    public function setWwwForStringSetsWww(): void
    {
        $this->subject->setWww('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('www'));
    }

    /**
     * @test
     */
    public function getEmailReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getEmail()
        );
    }

    /**
     * @test
     */
    public function setEmailForStringSetsEmail(): void
    {
        $this->subject->setEmail('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('email'));
    }

    /**
     * @test
     */
    public function getMembershipReturnsInitialValueForInt(): void
    {
        self::assertSame(
            0,
            $this->subject->getMembership()
        );
    }

    /**
     * @test
     */
    public function setMembershipForIntSetsMembership(): void
    {
        $this->subject->setMembership(12);

        self::assertEquals(12, $this->subject->_get('membership'));
    }

    /**
     * @test
     */
    public function getDirectionsReturnsInitialValueForString(): void
    {
        self::assertSame(
            '',
            $this->subject->getDirections()
        );
    }

    /**
     * @test
     */
    public function setDirectionsForStringSetsDirections(): void
    {
        $this->subject->setDirections('Conceived at T3CON10');

        self::assertEquals('Conceived at T3CON10', $this->subject->_get('directions'));
    }

    /**
     * @test
     */
    public function getLocationsReturnsInitialValueForLocation(): void
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getLocations()
        );
    }

    /**
     * @test
     */
    public function setLocationsForObjectStorageContainingLocationSetsLocations(): void
    {
        $location = new \T3graf\SisBase\Domain\Model\Location();
        $objectStorageHoldingExactlyOneLocations = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneLocations->attach($location);
        $this->subject->setLocations($objectStorageHoldingExactlyOneLocations);

        self::assertEquals($objectStorageHoldingExactlyOneLocations, $this->subject->_get('locations'));
    }

    /**
     * @test
     */
    public function addLocationToObjectStorageHoldingLocations(): void
    {
        $location = new \T3graf\SisBase\Domain\Model\Location();
        $locationsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $locationsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($location));
        $this->subject->_set('locations', $locationsObjectStorageMock);

        $this->subject->addLocation($location);
    }

    /**
     * @test
     */
    public function removeLocationFromObjectStorageHoldingLocations(): void
    {
        $location = new \T3graf\SisBase\Domain\Model\Location();
        $locationsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->onlyMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $locationsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($location));
        $this->subject->_set('locations', $locationsObjectStorageMock);

        $this->subject->removeLocation($location);
    }
}
