<?php
declare(strict_types=1);

namespace T3graf\SisBase\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author SIS Development Team <development@t3graf-media.de>
 */
class MatchesControllerTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Controller\MatchesController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\T3graf\SisBase\Controller\MatchesController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }



    /**
     * @test
     */
    public function createActionAddsTheGivenMatchesToMatchesRepository(): void
    {
        $matches = new \T3graf\SisBase\Domain\Model\Matches();

        $matchesRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\MatchesRepository::class)
            ->onlyMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $matchesRepository->expects(self::once())->method('add')->with($matches);
        $this->subject->_set('matchesRepository', $matchesRepository);

        $this->subject->createAction($matches);
    }

    
    /**
     * @test
     */
    public function updateActionUpdatesTheGivenMatchesInMatchesRepository(): void
    {
        $matches = new \T3graf\SisBase\Domain\Model\Matches();

        $matchesRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\MatchesRepository::class)
            ->onlyMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $matchesRepository->expects(self::once())->method('update')->with($matches);
        $this->subject->_set('matchesRepository', $matchesRepository);

        $this->subject->updateAction($matches);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenMatchesFromMatchesRepository(): void
    {
        $matches = new \T3graf\SisBase\Domain\Model\Matches();

        $matchesRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\MatchesRepository::class)
            ->onlyMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $matchesRepository->expects(self::once())->method('remove')->with($matches);
        $this->subject->_set('matchesRepository', $matchesRepository);

        $this->subject->deleteAction($matches);
    }
}
