<?php
declare(strict_types=1);

namespace T3graf\SisBase\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author SIS Development Team <development@t3graf-media.de>
 */
class EventControllerTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Controller\EventController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\T3graf\SisBase\Controller\EventController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }




    /**
     * @test
     */
    public function createActionAddsTheGivenEventToEventRepository(): void
    {
        $event = new \T3graf\SisBase\Domain\Model\Event();

        $eventRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\EventRepository::class)
            ->onlyMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $eventRepository->expects(self::once())->method('add')->with($event);
        $this->subject->_set('eventRepository', $eventRepository);

        $this->subject->createAction($event);
    }
}
