<?php
declare(strict_types=1);

namespace T3graf\SisBase\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author SIS Development Team <development@t3graf-media.de>
 */
class ProfilesControllerTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Controller\ProfilesController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\T3graf\SisBase\Controller\ProfilesController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }





    /**
     * @test
     */
    public function createActionAddsTheGivenProfilesToProfilesRepository(): void
    {
        $profiles = new \T3graf\SisBase\Domain\Model\Profiles();

        $profilesRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\ProfilesRepository::class)
            ->onlyMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $profilesRepository->expects(self::once())->method('add')->with($profiles);
        $this->subject->_set('profilesRepository', $profilesRepository);

        $this->subject->createAction($profiles);
    }


    /**
     * @test
     */
    public function updateActionUpdatesTheGivenProfilesInProfilesRepository(): void
    {
        $profiles = new \T3graf\SisBase\Domain\Model\Profiles();

        $profilesRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\ProfilesRepository::class)
            ->onlyMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $profilesRepository->expects(self::once())->method('update')->with($profiles);
        $this->subject->_set('profilesRepository', $profilesRepository);

        $this->subject->updateAction($profiles);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenProfilesFromProfilesRepository(): void
    {
        $profiles = new \T3graf\SisBase\Domain\Model\Profiles();

        $profilesRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\ProfilesRepository::class)
            ->onlyMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $profilesRepository->expects(self::once())->method('remove')->with($profiles);
        $this->subject->_set('profilesRepository', $profilesRepository);

        $this->subject->deleteAction($profiles);
    }
}
