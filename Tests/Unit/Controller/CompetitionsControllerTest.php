<?php
declare(strict_types=1);

namespace T3graf\SisBase\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author SIS Development Team <development@t3graf-media.de>
 */
class CompetitionsControllerTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Controller\CompetitionsController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\T3graf\SisBase\Controller\CompetitionsController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }





    /**
     * @test
     */
    public function createActionAddsTheGivenCompetitionsToCompetitionsRepository(): void
    {
        $competitions = new \T3graf\SisBase\Domain\Model\Competitions();

        $competitionsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\CompetitionsRepository::class)
            ->onlyMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $competitionsRepository->expects(self::once())->method('add')->with($competitions);
        $this->subject->_set('competitionsRepository', $competitionsRepository);

        $this->subject->createAction($competitions);
    }



    /**
     * @test
     */
    public function updateActionUpdatesTheGivenCompetitionsInCompetitionsRepository(): void
    {
        $competitions = new \T3graf\SisBase\Domain\Model\Competitions();

        $competitionsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\CompetitionsRepository::class)
            ->onlyMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $competitionsRepository->expects(self::once())->method('update')->with($competitions);
        $this->subject->_set('competitionsRepository', $competitionsRepository);

        $this->subject->updateAction($competitions);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenCompetitionsFromCompetitionsRepository(): void
    {
        $competitions = new \T3graf\SisBase\Domain\Model\Competitions();

        $competitionsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\CompetitionsRepository::class)
            ->onlyMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $competitionsRepository->expects(self::once())->method('remove')->with($competitions);
        $this->subject->_set('competitionsRepository', $competitionsRepository);

        $this->subject->deleteAction($competitions);
    }
}
