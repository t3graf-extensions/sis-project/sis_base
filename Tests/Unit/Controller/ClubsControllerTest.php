<?php
declare(strict_types=1);

namespace T3graf\SisBase\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author SIS Development Team <development@t3graf-media.de>
 */
class ClubsControllerTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Controller\ClubsController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\T3graf\SisBase\Controller\ClubsController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }




    /**
     * @test
     */
    public function createActionAddsTheGivenClubsToClubsRepository(): void
    {
        $clubs = new \T3graf\SisBase\Domain\Model\Clubs();

        $clubsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\ClubsRepository::class)
            ->onlyMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $clubsRepository->expects(self::once())->method('add')->with($clubs);
        $this->subject->_set('clubsRepository', $clubsRepository);

        $this->subject->createAction($clubs);
    }

    

    /**
     * @test
     */
    public function updateActionUpdatesTheGivenClubsInClubsRepository(): void
    {
        $clubs = new \T3graf\SisBase\Domain\Model\Clubs();

        $clubsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\ClubsRepository::class)
            ->onlyMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $clubsRepository->expects(self::once())->method('update')->with($clubs);
        $this->subject->_set('clubsRepository', $clubsRepository);

        $this->subject->updateAction($clubs);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenClubsFromClubsRepository(): void
    {
        $clubs = new \T3graf\SisBase\Domain\Model\Clubs();

        $clubsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\ClubsRepository::class)
            ->onlyMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $clubsRepository->expects(self::once())->method('remove')->with($clubs);
        $this->subject->_set('clubsRepository', $clubsRepository);

        $this->subject->deleteAction($clubs);
    }
}
