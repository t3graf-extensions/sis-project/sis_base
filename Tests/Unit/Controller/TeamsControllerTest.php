<?php
declare(strict_types=1);

namespace T3graf\SisBase\Tests\Unit\Controller;

use PHPUnit\Framework\MockObject\MockObject;
use TYPO3\TestingFramework\Core\AccessibleObjectInterface;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Test case
 *
 * @author SIS Development Team <development@t3graf-media.de>
 */
class TeamsControllerTest extends UnitTestCase
{
    /**
     * @var \T3graf\SisBase\Controller\TeamsController|MockObject|AccessibleObjectInterface
     */
    protected $subject;

    protected function setUp(): void
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder($this->buildAccessibleProxy(\T3graf\SisBase\Controller\TeamsController::class))
            ->onlyMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }





    /**
     * @test
     */
    public function createActionAddsTheGivenTeamsToTeamsRepository(): void
    {
        $teams = new \T3graf\SisBase\Domain\Model\Teams();

        $teamsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\TeamsRepository::class)
            ->onlyMethods(['add'])
            ->disableOriginalConstructor()
            ->getMock();

        $teamsRepository->expects(self::once())->method('add')->with($teams);
        $this->subject->_set('teamsRepository', $teamsRepository);

        $this->subject->createAction($teams);
    }



    /**
     * @test
     */
    public function updateActionUpdatesTheGivenTeamsInTeamsRepository(): void
    {
        $teams = new \T3graf\SisBase\Domain\Model\Teams();

        $teamsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\TeamsRepository::class)
            ->onlyMethods(['update'])
            ->disableOriginalConstructor()
            ->getMock();

        $teamsRepository->expects(self::once())->method('update')->with($teams);
        $this->subject->_set('teamsRepository', $teamsRepository);

        $this->subject->updateAction($teams);
    }

    /**
     * @test
     */
    public function deleteActionRemovesTheGivenTeamsFromTeamsRepository(): void
    {
        $teams = new \T3graf\SisBase\Domain\Model\Teams();

        $teamsRepository = $this->getMockBuilder(\T3graf\SisBase\Domain\Repository\TeamsRepository::class)
            ->onlyMethods(['remove'])
            ->disableOriginalConstructor()
            ->getMock();

        $teamsRepository->expects(self::once())->method('remove')->with($teams);
        $this->subject->_set('teamsRepository', $teamsRepository);

        $this->subject->deleteAction($teams);
    }
}
