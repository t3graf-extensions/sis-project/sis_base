<?php

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') || die();

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SisBase',
    'Reports',
    'SISReports'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'SisBase',
    'Matches',
    'SISMatches'
);
