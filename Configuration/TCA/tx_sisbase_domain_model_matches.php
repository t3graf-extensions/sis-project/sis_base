<?php

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    'ctrl' => [
        'title' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches',
        'label' => 'match_no',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'match_no,round_name,add_info,match_report,report_author,report_link',
        'iconfile' => 'EXT:sis_base/Resources/Public/Icons/tx_sisbase_domain_model_matches.svg',
    ],
    'types' => [
        '1' => ['showitem' => 'match_no, round, round_name, status, add_info, date, visitors, match_report, report_author, report_link, images, location, competition, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language, sys_language_uid, l10n_parent, l10n_diffsource, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_sisbase_domain_model_matches',
                'foreign_table_where' => 'AND {#tx_sisbase_domain_model_matches}.{#pid}=###CURRENT_PID### AND {#tx_sisbase_domain_model_matches}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true,
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038),
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],

        'match_no' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.match_no',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'round' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.round',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0,
            ],
        ],
        'round_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.round_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'status' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.status',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => '',
            ],
        ],
        'add_info' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.add_info',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'date' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.date',
            'config' => [
                'dbType' => 'datetime',
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 12,
                'eval' => 'datetime',
                'default' => null,
            ],
        ],
        'visitors' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.visitors',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0,
            ],
        ],
        'match_report' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.match_report',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'report_author' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.report_author',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'report_link' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.report_link',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputLink',
            ],
        ],
        'images' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'images',
                        'tablenames' => 'tx_sisbase_domain_model_matches',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1,
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),

        ],
        'location' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.location',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_sisbase_domain_model_location',
                'default' => 0,
                'minitems' => 0,
                'maxitems' => 1,
            ],

        ],
        'competition' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_matches.competition',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_sisbase_domain_model_competitions',
                'default' => 0,
                'minitems' => 0,
                'maxitems' => 1,
            ],

        ],

    ],
];
