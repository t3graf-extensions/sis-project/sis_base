<?php

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    'ctrl' => [
        'title' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'name,short_name,street,zip,city,country,country_code,short_info,description,club_colors,www,email,directions',
        'iconfile' => 'EXT:sis_base/Resources/Public/Icons/tx_sisbase_domain_model_clubs.svg',
    ],
    'types' => [
        '1' => ['showitem' => 'name, short_name, logo, street, zip, city, country, country_code, lng, lat, short_info, description, founding_year, club_colors, www, email, membership, directions, locations, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:language, sys_language_uid, l10n_parent, l10n_diffsource, --div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:access, hidden, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'language',
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_sisbase_domain_model_clubs',
                'foreign_table_where' => 'AND {#tx_sisbase_domain_model_clubs}.{#pid}=###CURRENT_PID### AND {#tx_sisbase_domain_model_clubs}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true,
                    ],
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038),
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true,
                ],
            ],
        ],

        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'short_name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.short_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'logo' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.logo',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'logo',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference',
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette',
                        ],
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'logo',
                        'tablenames' => 'tx_sisbase_domain_model_clubs',
                        'table_local' => 'sys_file',
                    ],
                    'maxitems' => 1,
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),

        ],
        'street' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.street',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'zip' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.zip',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'city' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.city',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'country' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.country',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'country_code' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.country_code',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'lng' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.lng',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2',
            ],
        ],
        'lat' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.lat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2',
            ],
        ],
        'short_info' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.short_info',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'description' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.description',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'founding_year' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.founding_year',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0,
            ],
        ],
        'club_colors' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.club_colors',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'www' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.www',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'email' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim',
                'default' => '',
            ],
        ],
        'membership' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.membership',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int',
                'default' => 0,
            ],
        ],
        'directions' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.directions',
            'config' => [
                'type' => 'text',
                'enableRichtext' => true,
                'richtextConfiguration' => 'default',
                'fieldControl' => [
                    'fullScreenRichtext' => [
                        'disabled' => false,
                    ],
                ],
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],

        ],
        'locations' => [
            'exclude' => true,
            'label' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_db.xlf:tx_sisbase_domain_model_clubs.locations',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_sisbase_domain_model_location',
                'MM' => 'tx_sisbase_clubs_location_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],

        ],

    ],
];
