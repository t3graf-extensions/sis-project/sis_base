<?php

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

return [
    'sis_base-plugin-reports' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:sis_base/Resources/Public/Icons/user_plugin_reports.svg',
    ],
    'sis_base-plugin-matches' => [
        'provider' => \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
        'source' => 'EXT:sis_base/Resources/Public/Icons/user_plugin_matches.svg',
    ],
];
