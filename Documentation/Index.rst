.. every .rst file should include Includes.txt
.. use correct path!

.. include:: Includes.txt

.. Every manual should have a start label for cross-referencing to
.. start page. Do not remove this!

.. _start:

=============================================================
SIS - Sport Information System for TYPO3
=============================================================

:Version:     |release|
:Language:    en
:Description: Sports information system for sports clubs to present team information, player profiles, upcoming matchdays, game results, competition or tournament tables, reports and statistics on the club website.

   **Importend!!!**

   This extension only provides SIS backend modules and several basic plugin functions (team list view, team detail view, player detail view etc.)

   **For full use you have to choose the SIS extension for your sport.**
:Keywords:     TYPO3 CMS, Sports, Sportclubs, Competitions, Tournaments
:Copyright:    2020-2022
:Authors:      SIS Development Team
:Email:        development@t3graf-media.de
:License:      This extension documentation is published under the `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/>`__ (Creative Commons) license
:Rendered:     |today|

The content of this document is related to TYPO3 CMS,
a GNU/GPL CMS/Framework available from `typo3.org <https://typo3.org/>`_ .


**Table of Contents**

.. toctree::
   :maxdepth: 3

   Introduction/Index
   Editor/Index
   Installation/Index
   Configuration/Index
   Developer/Index
   KnownProblems/Index
   ChangeLog/Index
   Sitemap
