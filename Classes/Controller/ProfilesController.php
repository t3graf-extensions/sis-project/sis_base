<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Controller;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * ProfilesController
 */
class ProfilesController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * profilesRepository
     *
     * @var \T3graf\SisBase\Domain\Repository\ProfilesRepository
     */
    protected $profilesRepository;

    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction(): \Psr\Http\Message\ResponseInterface
    {
        $profiles = $this->profilesRepository->findAll();
        $this->view->assign('profiles', $profiles);
        return $this->htmlResponse();
    }

    /**
     * action show
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $profiles
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function showAction(\T3graf\SisBase\Domain\Model\Profiles $profiles): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('profiles', $profiles);
        return $this->htmlResponse();
    }

    /**
     * action new
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function newAction(): \Psr\Http\Message\ResponseInterface
    {
        return $this->htmlResponse();
    }

    /**
     * action create
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $newProfiles
     */
    public function createAction(\T3graf\SisBase\Domain\Model\Profiles $newProfiles)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->profilesRepository->add($newProfiles);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $profiles
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("profiles")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function editAction(\T3graf\SisBase\Domain\Model\Profiles $profiles): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('profiles', $profiles);
        return $this->htmlResponse();
    }

    /**
     * action update
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $profiles
     */
    public function updateAction(\T3graf\SisBase\Domain\Model\Profiles $profiles)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->profilesRepository->update($profiles);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $profiles
     */
    public function deleteAction(\T3graf\SisBase\Domain\Model\Profiles $profiles)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->profilesRepository->remove($profiles);
        $this->redirect('list');
    }

    /**
     * @param \T3graf\SisBase\Domain\Repository\ProfilesRepository $profilesRepository
     */
    public function injectProfilesRepository(\T3graf\SisBase\Domain\Repository\ProfilesRepository $profilesRepository)
    {
        $this->profilesRepository = $profilesRepository;
    }
}
