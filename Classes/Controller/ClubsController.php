<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Controller;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * ClubsController
 */
class ClubsController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * clubsRepository
     *
     * @var \T3graf\SisBase\Domain\Repository\ClubsRepository
     */
    protected $clubsRepository;

    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction(): \Psr\Http\Message\ResponseInterface
    {
        $clubs = $this->clubsRepository->findAll();
        $this->view->assign('clubs', $clubs);
        return $this->htmlResponse();
    }

    /**
     * action show
     *
     * @param \T3graf\SisBase\Domain\Model\Clubs $clubs
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function showAction(\T3graf\SisBase\Domain\Model\Clubs $clubs): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('clubs', $clubs);
        return $this->htmlResponse();
    }

    /**
     * action new
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function newAction(): \Psr\Http\Message\ResponseInterface
    {
        return $this->htmlResponse();
    }

    /**
     * action create
     *
     * @param \T3graf\SisBase\Domain\Model\Clubs $newClubs
     */
    public function createAction(\T3graf\SisBase\Domain\Model\Clubs $newClubs)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->clubsRepository->add($newClubs);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \T3graf\SisBase\Domain\Model\Clubs $clubs
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("clubs")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function editAction(\T3graf\SisBase\Domain\Model\Clubs $clubs): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('clubs', $clubs);
        return $this->htmlResponse();
    }

    /**
     * action update
     *
     * @param \T3graf\SisBase\Domain\Model\Clubs $clubs
     */
    public function updateAction(\T3graf\SisBase\Domain\Model\Clubs $clubs)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->clubsRepository->update($clubs);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \T3graf\SisBase\Domain\Model\Clubs $clubs
     */
    public function deleteAction(\T3graf\SisBase\Domain\Model\Clubs $clubs)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->clubsRepository->remove($clubs);
        $this->redirect('list');
    }

    /**
     * @param \T3graf\SisBase\Domain\Repository\ClubsRepository $clubsRepository
     */
    public function injectClubsRepository(\T3graf\SisBase\Domain\Repository\ClubsRepository $clubsRepository)
    {
        $this->clubsRepository = $clubsRepository;
    }
}
