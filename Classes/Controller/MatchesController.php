<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Controller;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * MatchesController
 */
class MatchesController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * matchesRepository
     *
     * @var \T3graf\SisBase\Domain\Repository\MatchesRepository
     */
    protected $matchesRepository;

    /**
     * action list
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function listAction(): \Psr\Http\Message\ResponseInterface
    {
        $matches = $this->matchesRepository->findAll();
        $this->view->assign('matches', $matches);
        return $this->htmlResponse();
    }

    /**
     * action show
     *
     * @param \T3graf\SisBase\Domain\Model\Matches $matches
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function showAction(\T3graf\SisBase\Domain\Model\Matches $matches): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('matches', $matches);
        return $this->htmlResponse();
    }

    /**
     * action new
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function newAction(): \Psr\Http\Message\ResponseInterface
    {
        return $this->htmlResponse();
    }

    /**
     * action create
     *
     * @param \T3graf\SisBase\Domain\Model\Matches $newMatches
     */
    public function createAction(\T3graf\SisBase\Domain\Model\Matches $newMatches)
    {
        $this->addFlashMessage('The object was created. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->matchesRepository->add($newMatches);
        $this->redirect('list');
    }

    /**
     * action edit
     *
     * @param \T3graf\SisBase\Domain\Model\Matches $matches
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation("matches")
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function editAction(\T3graf\SisBase\Domain\Model\Matches $matches): \Psr\Http\Message\ResponseInterface
    {
        $this->view->assign('matches', $matches);
        return $this->htmlResponse();
    }

    /**
     * action update
     *
     * @param \T3graf\SisBase\Domain\Model\Matches $matches
     */
    public function updateAction(\T3graf\SisBase\Domain\Model\Matches $matches)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->matchesRepository->update($matches);
        $this->redirect('list');
    }

    /**
     * action delete
     *
     * @param \T3graf\SisBase\Domain\Model\Matches $matches
     */
    public function deleteAction(\T3graf\SisBase\Domain\Model\Matches $matches)
    {
        $this->addFlashMessage('The object was deleted. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/p/friendsoftypo3/extension-builder/master/en-us/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->matchesRepository->remove($matches);
        $this->redirect('list');
    }

    /**
     * @param \T3graf\SisBase\Domain\Repository\MatchesRepository $matchesRepository
     */
    public function injectMatchesRepository(\T3graf\SisBase\Domain\Repository\MatchesRepository $matchesRepository)
    {
        $this->matchesRepository = $matchesRepository;
    }
}
