<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Domain\Model;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Some collections of competitions
 */
class Event extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * competitions
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Competitions>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $competitions;

    /**
     * __construct
     */
    public function __construct()
    {

        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     */
    public function initializeObject()
    {
        $this->competitions = $this->competitions ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Adds a Competitions
     *
     * @param \T3graf\SisBase\Domain\Model\Competitions $competition
     */
    public function addCompetition(\T3graf\SisBase\Domain\Model\Competitions $competition)
    {
        $this->competitions->attach($competition);
    }

    /**
     * Removes a Competitions
     *
     * @param \T3graf\SisBase\Domain\Model\Competitions $competitionToRemove The Competitions to be removed
     */
    public function removeCompetition(\T3graf\SisBase\Domain\Model\Competitions $competitionToRemove)
    {
        $this->competitions->detach($competitionToRemove);
    }

    /**
     * Returns the competitions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Competitions> $competitions
     */
    public function getCompetitions()
    {
        return $this->competitions;
    }

    /**
     * Sets the competitions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Competitions> $competitions
     */
    public function setCompetitions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $competitions)
    {
        $this->competitions = $competitions;
    }
}
