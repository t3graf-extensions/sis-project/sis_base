<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Domain\Model;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Table of teams. For sports with team competitions.
 */
class Teams extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * shortName
     *
     * @var string
     */
    protected $shortName = '';

    /**
     * logo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $logo;

    /**
     * images
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images;

    /**
     * teamComment
     *
     * @var string
     */
    protected $teamComment = '';

    /**
     * playersComment
     *
     * @var string
     */
    protected $playersComment = '';

    /**
     * coachesComment
     *
     * @var string
     */
    protected $coachesComment = '';

    /**
     * supervisorsComment
     *
     * @var string
     */
    protected $supervisorsComment = '';

    /**
     * club
     *
     * @var \T3graf\SisBase\Domain\Model\Clubs
     */
    protected $club;

    /**
     * ageGroup
     *
     * @var \T3graf\SisBase\Domain\Model\AgeGroups
     */
    protected $ageGroup;

    /**
     * coaches
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $coaches;

    /**
     * players
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $players;

    /**
     * supervisors
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $supervisors;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Returns the shortName
     *
     * @return string $shortName
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Sets the shortName
     *
     * @param string $shortName
     */
    public function setShortName(string $shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * Returns the logo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function setImages(\TYPO3\CMS\Extbase\Domain\Model\FileReference $images)
    {
        $this->images = $images;
    }

    /**
     * Returns the teamComment
     *
     * @return string $teamComment
     */
    public function getTeamComment()
    {
        return $this->teamComment;
    }

    /**
     * Sets the teamComment
     *
     * @param string $teamComment
     */
    public function setTeamComment(string $teamComment)
    {
        $this->teamComment = $teamComment;
    }

    /**
     * Returns the playersComment
     *
     * @return string $playersComment
     */
    public function getPlayersComment()
    {
        return $this->playersComment;
    }

    /**
     * Sets the playersComment
     *
     * @param string $playersComment
     */
    public function setPlayersComment(string $playersComment)
    {
        $this->playersComment = $playersComment;
    }

    /**
     * Returns the coachesComment
     *
     * @return string $coachesComment
     */
    public function getCoachesComment()
    {
        return $this->coachesComment;
    }

    /**
     * Sets the coachesComment
     *
     * @param string $coachesComment
     */
    public function setCoachesComment(string $coachesComment)
    {
        $this->coachesComment = $coachesComment;
    }

    /**
     * Returns the supervisorsComment
     *
     * @return string $supervisorsComment
     */
    public function getSupervisorsComment()
    {
        return $this->supervisorsComment;
    }

    /**
     * Sets the supervisorsComment
     *
     * @param string $supervisorsComment
     */
    public function setSupervisorsComment(string $supervisorsComment)
    {
        $this->supervisorsComment = $supervisorsComment;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     */
    public function initializeObject()
    {
        $this->coaches = $this->coaches ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->players = $this->players ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->supervisors = $this->supervisors ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the ageGroup
     *
     * @return \T3graf\SisBase\Domain\Model\AgeGroups $ageGroup
     */
    public function getAgeGroup()
    {
        return $this->ageGroup;
    }

    /**
     * Sets the ageGroup
     *
     * @param \T3graf\SisBase\Domain\Model\AgeGroups $ageGroup
     */
    public function setAgeGroup(\T3graf\SisBase\Domain\Model\AgeGroups $ageGroup)
    {
        $this->ageGroup = $ageGroup;
    }

    /**
     * Adds a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $coach
     */
    public function addCoach(\T3graf\SisBase\Domain\Model\Profiles $coach)
    {
        $this->coaches->attach($coach);
    }

    /**
     * Removes a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $coachToRemove The Profiles to be removed
     */
    public function removeCoach(\T3graf\SisBase\Domain\Model\Profiles $coachToRemove)
    {
        $this->coaches->detach($coachToRemove);
    }

    /**
     * Returns the coaches
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $coaches
     */
    public function getCoaches()
    {
        return $this->coaches;
    }

    /**
     * Sets the coaches
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $coaches
     */
    public function setCoaches(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $coaches)
    {
        $this->coaches = $coaches;
    }

    /**
     * Adds a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $player
     */
    public function addPlayer(\T3graf\SisBase\Domain\Model\Profiles $player)
    {
        $this->players->attach($player);
    }

    /**
     * Removes a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $playerToRemove The Profiles to be removed
     */
    public function removePlayer(\T3graf\SisBase\Domain\Model\Profiles $playerToRemove)
    {
        $this->players->detach($playerToRemove);
    }

    /**
     * Returns the players
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $players
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Sets the players
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $players
     */
    public function setPlayers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $players)
    {
        $this->players = $players;
    }

    /**
     * Adds a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $supervisor
     */
    public function addSupervisor(\T3graf\SisBase\Domain\Model\Profiles $supervisor)
    {
        $this->supervisors->attach($supervisor);
    }

    /**
     * Removes a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $supervisorToRemove The Profiles to be removed
     */
    public function removeSupervisor(\T3graf\SisBase\Domain\Model\Profiles $supervisorToRemove)
    {
        $this->supervisors->detach($supervisorToRemove);
    }

    /**
     * Returns the supervisors
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $supervisors
     */
    public function getSupervisors()
    {
        return $this->supervisors;
    }

    /**
     * Sets the supervisors
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $supervisors
     */
    public function setSupervisors(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $supervisors)
    {
        $this->supervisors = $supervisors;
    }

    /**
     * Returns the club
     *
     * @return \T3graf\SisBase\Domain\Model\Clubs $club
     */
    public function getClub()
    {
        return $this->club;
    }

    /**
     * Sets the club
     *
     * @param \T3graf\SisBase\Domain\Model\Clubs $club
     */
    public function setClub(\T3graf\SisBase\Domain\Model\Clubs $club)
    {
        $this->club = $club;
    }
}
