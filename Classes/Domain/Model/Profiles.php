<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Domain\Model;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Profiles of players, coaches and so on.
 */
class Profiles extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * firstName
     *
     * @var string
     */
    protected $firstName = '';

    /**
     * lastName
     *
     * @var string
     */
    protected $lastName = '';

    /**
     * stageName
     *
     * @var string
     */
    protected $stageName = '';

    /**
     * profileImages
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $profileImages;

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * birthday
     *
     * @var \DateTime
     */
    protected $birthday;

    /**
     * birthplace
     *
     * @var string
     */
    protected $birthplace = '';

    /**
     * nationality
     *
     * @var string
     */
    protected $nationality = '';

    /**
     * gender
     *
     * @var int
     */
    protected $gender = 0;

    /**
     * height
     *
     * @var string
     */
    protected $height = '';

    /**
     * weight
     *
     * @var string
     */
    protected $weight = '';

    /**
     * contractStart
     *
     * @var \DateTime
     */
    protected $contractStart;

    /**
     * contractEnd
     *
     * @var \DateTime
     */
    protected $contractEnd;

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * nickname
     *
     * @var string
     */
    protected $nickname = '';

    /**
     * gdpr
     *
     * @var int
     */
    protected $gdpr = 0;

    /**
     * images
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images;

    /**
     * lastClubs
     *
     * @var string
     */
    protected $lastClubs = '';

    /**
     * shortDescription
     *
     * @var string
     */
    protected $shortDescription = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * profileTyp
     *
     * @var int
     */
    protected $profileTyp = 0;

    /**
     * jerseyNumber
     *
     * @var int
     */
    protected $jerseyNumber = 0;

    /**
     * position
     *
     * @var string
     */
    protected $position = '';

    /**
     * Returns the firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName
     *
     * @param string $firstName
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Returns the lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the lastName
     *
     * @param string $lastName
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Returns the stageName
     *
     * @return string $stageName
     */
    public function getStageName()
    {
        return $this->stageName;
    }

    /**
     * Sets the stageName
     *
     * @param string $stageName
     */
    public function setStageName(string $stageName)
    {
        $this->stageName = $stageName;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * Returns the birthday
     *
     * @return \DateTime $birthday
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Sets the birthday
     *
     * @param \DateTime $birthday
     */
    public function setBirthday(\DateTime $birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * Returns the birthplace
     *
     * @return string $birthplace
     */
    public function getBirthplace()
    {
        return $this->birthplace;
    }

    /**
     * Sets the birthplace
     *
     * @param string $birthplace
     */
    public function setBirthplace(string $birthplace)
    {
        $this->birthplace = $birthplace;
    }

    /**
     * Returns the nationality
     *
     * @return string $nationality
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Sets the nationality
     *
     * @param string $nationality
     */
    public function setNationality(string $nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * Returns the gender
     *
     * @return int $gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the gender
     *
     * @param int $gender
     */
    public function setGender(int $gender)
    {
        $this->gender = $gender;
    }

    /**
     * Returns the height
     *
     * @return string $height
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Sets the height
     *
     * @param string $height
     */
    public function setHeight(string $height)
    {
        $this->height = $height;
    }

    /**
     * Returns the weight
     *
     * @return string $weight
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Sets the weight
     *
     * @param string $weight
     */
    public function setWeight(string $weight)
    {
        $this->weight = $weight;
    }

    /**
     * Returns the contractStart
     *
     * @return \DateTime $contractStart
     */
    public function getContractStart()
    {
        return $this->contractStart;
    }

    /**
     * Sets the contractStart
     *
     * @param \DateTime $contractStart
     */
    public function setContractStart(\DateTime $contractStart)
    {
        $this->contractStart = $contractStart;
    }

    /**
     * Returns the contractEnd
     *
     * @return \DateTime $contractEnd
     */
    public function getContractEnd()
    {
        return $this->contractEnd;
    }

    /**
     * Sets the contractEnd
     *
     * @param \DateTime $contractEnd
     */
    public function setContractEnd(\DateTime $contractEnd)
    {
        $this->contractEnd = $contractEnd;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Returns the nickname
     *
     * @return string $nickname
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Sets the nickname
     *
     * @param string $nickname
     */
    public function setNickname(string $nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * Returns the gdpr
     *
     * @return int $gdpr
     */
    public function getGdpr()
    {
        return $this->gdpr;
    }

    /**
     * Sets the gdpr
     *
     * @param int $gdpr
     */
    public function setGdpr(int $gdpr)
    {
        $this->gdpr = $gdpr;
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function setImages(\TYPO3\CMS\Extbase\Domain\Model\FileReference $images)
    {
        $this->images = $images;
    }

    /**
     * Returns the shortDescription
     *
     * @return string $shortDescription
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Sets the shortDescription
     *
     * @param string $shortDescription
     */
    public function setShortDescription(string $shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * Returns the profileTyp
     *
     * @return int $profileTyp
     */
    public function getProfileTyp()
    {
        return $this->profileTyp;
    }

    /**
     * Sets the profileTyp
     *
     * @param int $profileTyp
     */
    public function setProfileTyp(int $profileTyp)
    {
        $this->profileTyp = $profileTyp;
    }

    /**
     * Returns the profileImages
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImages
     */
    public function getProfileImages()
    {
        return $this->profileImages;
    }

    /**
     * Sets the profileImages
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImages
     */
    public function setProfileImages(\TYPO3\CMS\Extbase\Domain\Model\FileReference $profileImages)
    {
        $this->profileImages = $profileImages;
    }

    /**
     * Returns the lastClubs
     *
     * @return string $lastClubs
     */
    public function getLastClubs()
    {
        return $this->lastClubs;
    }

    /**
     * Sets the lastClubs
     *
     * @param string $lastClubs
     */
    public function setLastClubs(string $lastClubs)
    {
        $this->lastClubs = $lastClubs;
    }

    /**
     * Returns the jerseyNumber
     *
     * @return int $jerseyNumber
     */
    public function getJerseyNumber()
    {
        return $this->jerseyNumber;
    }

    /**
     * Sets the jerseyNumber
     *
     * @param int $jerseyNumber
     */
    public function setJerseyNumber(int $jerseyNumber)
    {
        $this->jerseyNumber = $jerseyNumber;
    }

    /**
     * Returns the position
     *
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the position
     *
     * @param string $position
     */
    public function setPosition(string $position)
    {
        $this->position = $position;
    }
}
