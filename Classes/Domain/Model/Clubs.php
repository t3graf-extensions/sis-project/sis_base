<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Domain\Model;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Table of clubs information
 */
class Clubs extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * shortName
     *
     * @var string
     */
    protected $shortName = '';

    /**
     * logo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $logo;

    /**
     * street
     *
     * @var string
     */
    protected $street = '';

    /**
     * zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * country
     *
     * @var string
     */
    protected $country = '';

    /**
     * countryCode
     *
     * @var string
     */
    protected $countryCode = '';

    /**
     * lng
     *
     * @var float
     */
    protected $lng = 0.0;

    /**
     * lat
     *
     * @var float
     */
    protected $lat = 0.0;

    /**
     * shortInfo
     *
     * @var string
     */
    protected $shortInfo = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * foundingYear
     *
     * @var int
     */
    protected $foundingYear = 0;

    /**
     * clubColors
     *
     * @var string
     */
    protected $clubColors = '';

    /**
     * www
     *
     * @var string
     */
    protected $www = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * membership
     *
     * @var int
     */
    protected $membership = 0;

    /**
     * directions
     *
     * @var string
     */
    protected $directions = '';

    /**
     * locations
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Location>
     */
    protected $locations;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Returns the shortName
     *
     * @return string $shortName
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Sets the shortName
     *
     * @param string $shortName
     */
    public function setShortName(string $shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * Returns the logo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Returns the street
     *
     * @return string $street
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Sets the street
     *
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     */
    public function setZip(string $zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     */
    public function setCity(string $city)
    {
        $this->city = $city;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     */
    public function setCountry(string $country)
    {
        $this->country = $country;
    }

    /**
     * Returns the countryCode
     *
     * @return string $countryCode
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Sets the countryCode
     *
     * @param string $countryCode
     */
    public function setCountryCode(string $countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * Returns the lng
     *
     * @return float $lng
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Sets the lng
     *
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->lng = $lng;
    }

    /**
     * Returns the lat
     *
     * @return float $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Sets the lat
     *
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->lat = $lat;
    }

    /**
     * Returns the shortInfo
     *
     * @return string $shortInfo
     */
    public function getShortInfo()
    {
        return $this->shortInfo;
    }

    /**
     * Sets the shortInfo
     *
     * @param string $shortInfo
     */
    public function setShortInfo(string $shortInfo)
    {
        $this->shortInfo = $shortInfo;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * Returns the foundingYear
     *
     * @return int $foundingYear
     */
    public function getFoundingYear()
    {
        return $this->foundingYear;
    }

    /**
     * Sets the foundingYear
     *
     * @param int $foundingYear
     */
    public function setFoundingYear(int $foundingYear)
    {
        $this->foundingYear = $foundingYear;
    }

    /**
     * Returns the clubColors
     *
     * @return string $clubColors
     */
    public function getClubColors()
    {
        return $this->clubColors;
    }

    /**
     * Sets the clubColors
     *
     * @param string $clubColors
     */
    public function setClubColors(string $clubColors)
    {
        $this->clubColors = $clubColors;
    }

    /**
     * Returns the www
     *
     * @return string $www
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Sets the www
     *
     * @param string $www
     */
    public function setWww(string $www)
    {
        $this->www = $www;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Returns the membership
     *
     * @return int $membership
     */
    public function getMembership()
    {
        return $this->membership;
    }

    /**
     * Sets the membership
     *
     * @param int $membership
     */
    public function setMembership(int $membership)
    {
        $this->membership = $membership;
    }

    /**
     * Returns the directions
     *
     * @return string $directions
     */
    public function getDirections()
    {
        return $this->directions;
    }

    /**
     * Sets the directions
     *
     * @param string $directions
     */
    public function setDirections(string $directions)
    {
        $this->directions = $directions;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     */
    public function initializeObject()
    {
        $this->locations = $this->locations ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Location
     *
     * @param \T3graf\SisBase\Domain\Model\Location $location
     */
    public function addLocation(\T3graf\SisBase\Domain\Model\Location $location)
    {
        $this->locations->attach($location);
    }

    /**
     * Removes a Location
     *
     * @param \T3graf\SisBase\Domain\Model\Location $locationToRemove The Location to be removed
     */
    public function removeLocation(\T3graf\SisBase\Domain\Model\Location $locationToRemove)
    {
        $this->locations->detach($locationToRemove);
    }

    /**
     * Returns the locations
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Location> $locations
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Sets the locations
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Location> $locations
     */
    public function setLocations(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $locations)
    {
        $this->locations = $locations;
    }
}
