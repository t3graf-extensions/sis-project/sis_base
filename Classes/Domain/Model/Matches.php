<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Domain\Model;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Matches
 */
class Matches extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * matchNo
     *
     * @var string
     */
    protected $matchNo = '';

    /**
     * round
     *
     * @var int
     */
    protected $round = 0;

    /**
     * roundName
     *
     * @var string
     */
    protected $roundName = '';

    /**
     * status
     *
     * @var int
     */
    protected $status = 0;

    /**
     * addInfo
     *
     * @var string
     */
    protected $addInfo = '';

    /**
     * date
     *
     * @var \DateTime
     */
    protected $date;

    /**
     * visitors
     *
     * @var int
     */
    protected $visitors = 0;

    /**
     * matchReport
     *
     * @var string
     */
    protected $matchReport = '';

    /**
     * reportAuthor
     *
     * @var string
     */
    protected $reportAuthor = '';

    /**
     * reportLink
     *
     * @var string
     */
    protected $reportLink = '';

    /**
     * images
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images;

    /**
     * location
     *
     * @var \T3graf\SisBase\Domain\Model\Location
     */
    protected $location;

    /**
     * competition
     *
     * @var \T3graf\SisBase\Domain\Model\Competitions
     */
    protected $competition;

    /**
     * Returns the matchNo
     *
     * @return string $matchNo
     */
    public function getMatchNo()
    {
        return $this->matchNo;
    }

    /**
     * Sets the matchNo
     *
     * @param string $matchNo
     */
    public function setMatchNo(string $matchNo)
    {
        $this->matchNo = $matchNo;
    }

    /**
     * Returns the round
     *
     * @return int $round
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Sets the round
     *
     * @param int $round
     */
    public function setRound(int $round)
    {
        $this->round = $round;
    }

    /**
     * Returns the roundName
     *
     * @return string $roundName
     */
    public function getRoundName()
    {
        return $this->roundName;
    }

    /**
     * Sets the roundName
     *
     * @param string $roundName
     */
    public function setRoundName(string $roundName)
    {
        $this->roundName = $roundName;
    }

    /**
     * Returns the status
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    /**
     * Returns the addInfo
     *
     * @return string $addInfo
     */
    public function getAddInfo()
    {
        return $this->addInfo;
    }

    /**
     * Sets the addInfo
     *
     * @param string $addInfo
     */
    public function setAddInfo(string $addInfo)
    {
        $this->addInfo = $addInfo;
    }

    /**
     * Returns the date
     *
     * @return \DateTime $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Sets the date
     *
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Returns the visitors
     *
     * @return int $visitors
     */
    public function getVisitors()
    {
        return $this->visitors;
    }

    /**
     * Sets the visitors
     *
     * @param int $visitors
     */
    public function setVisitors(int $visitors)
    {
        $this->visitors = $visitors;
    }

    /**
     * Returns the matchReport
     *
     * @return string $matchReport
     */
    public function getMatchReport()
    {
        return $this->matchReport;
    }

    /**
     * Sets the matchReport
     *
     * @param string $matchReport
     */
    public function setMatchReport(string $matchReport)
    {
        $this->matchReport = $matchReport;
    }

    /**
     * Returns the reportAuthor
     *
     * @return string $reportAuthor
     */
    public function getReportAuthor()
    {
        return $this->reportAuthor;
    }

    /**
     * Sets the reportAuthor
     *
     * @param string $reportAuthor
     */
    public function setReportAuthor(string $reportAuthor)
    {
        $this->reportAuthor = $reportAuthor;
    }

    /**
     * Returns the reportLink
     *
     * @return string $reportLink
     */
    public function getReportLink()
    {
        return $this->reportLink;
    }

    /**
     * Sets the reportLink
     *
     * @param string $reportLink
     */
    public function setReportLink(string $reportLink)
    {
        $this->reportLink = $reportLink;
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $images
     */
    public function setImages(\TYPO3\CMS\Extbase\Domain\Model\FileReference $images)
    {
        $this->images = $images;
    }

    /**
     * Returns the location
     *
     * @return \T3graf\SisBase\Domain\Model\Location $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param \T3graf\SisBase\Domain\Model\Location $location
     */
    public function setLocation(\T3graf\SisBase\Domain\Model\Location $location)
    {
        $this->location = $location;
    }

    /**
     * Returns the competition
     *
     * @return \T3graf\SisBase\Domain\Model\Competitions $competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Sets the competition
     *
     * @param \T3graf\SisBase\Domain\Model\Competitions $competition
     */
    public function setCompetition(\T3graf\SisBase\Domain\Model\Competitions $competition)
    {
        $this->competition = $competition;
    }
}
