<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Domain\Model;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Season of competition
 */
class Season extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * winterBreak
     *
     * @var \DateTime
     */
    protected $winterBreak;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Returns the winterBreak
     *
     * @return \DateTime $winterBreak
     */
    public function getWinterBreak()
    {
        return $this->winterBreak;
    }

    /**
     * Sets the winterBreak
     *
     * @param \DateTime $winterBreak
     */
    public function setWinterBreak(\DateTime $winterBreak)
    {
        $this->winterBreak = $winterBreak;
    }
}
