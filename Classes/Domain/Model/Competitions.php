<?php

declare(strict_types=1);

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

namespace T3graf\SisBase\Domain\Model;

/**
 * This file is part of the "SIS - Sport Information System for TYPO3" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2022 SIS Development Team <development@t3graf-media.de>, T3graf media-agentur UG
 */

/**
 * Competitions
 */
class Competitions extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * shortName
     *
     * @var string
     */
    protected $shortName = '';

    /**
     * logo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $logo;

    /**
     * sports
     *
     * @var int
     */
    protected $sports = 0;

    /**
     * competitionMode
     *
     * @var int
     */
    protected $competitionMode = 0;

    /**
     * competitionTyp
     *
     * @var int
     */
    protected $competitionTyp = 0;

    /**
     * ageGroups
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\AgeGroups>
     */
    protected $ageGroups;

    /**
     * season
     *
     * @var \T3graf\SisBase\Domain\Model\Season
     */
    protected $season;

    /**
     * teams
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Teams>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $teams;

    /**
     * players
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $players;

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * __construct
     */
    public function __construct()
    {

        // Do not remove the next line: It would break the functionality
        $this->initializeObject();
    }

    /**
     * Initializes all ObjectStorage properties when model is reconstructed from DB (where __construct is not called)
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     */
    public function initializeObject()
    {
        $this->ageGroups = $this->ageGroups ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->teams = $this->teams ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->players = $this->players ?: new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the shortName
     *
     * @return string $shortName
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * Sets the shortName
     *
     * @param string $shortName
     */
    public function setShortName(string $shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * Returns the competitionTyp
     *
     * @return int $competitionTyp
     */
    public function getCompetitionTyp()
    {
        return $this->competitionTyp;
    }

    /**
     * Sets the competitionTyp
     *
     * @param int $competitionTyp
     */
    public function setCompetitionTyp(int $competitionTyp)
    {
        $this->competitionTyp = $competitionTyp;
    }

    /**
     * Returns the logo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Returns the sports
     *
     * @return int $sports
     */
    public function getSports()
    {
        return $this->sports;
    }

    /**
     * Sets the sports
     *
     * @param int $sports
     */
    public function setSports(int $sports)
    {
        $this->sports = $sports;
    }

    /**
     * Adds a AgeGroups
     *
     * @param \T3graf\SisBase\Domain\Model\AgeGroups $ageGroup
     */
    public function addAgeGroup(\T3graf\SisBase\Domain\Model\AgeGroups $ageGroup)
    {
        $this->ageGroups->attach($ageGroup);
    }

    /**
     * Removes a AgeGroups
     *
     * @param \T3graf\SisBase\Domain\Model\AgeGroups $ageGroupToRemove The AgeGroups to be removed
     */
    public function removeAgeGroup(\T3graf\SisBase\Domain\Model\AgeGroups $ageGroupToRemove)
    {
        $this->ageGroups->detach($ageGroupToRemove);
    }

    /**
     * Returns the ageGroups
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\AgeGroups> $ageGroups
     */
    public function getAgeGroups()
    {
        return $this->ageGroups;
    }

    /**
     * Sets the ageGroups
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\AgeGroups> $ageGroups
     */
    public function setAgeGroups(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $ageGroups)
    {
        $this->ageGroups = $ageGroups;
    }

    /**
     * Returns the season
     *
     * @return \T3graf\SisBase\Domain\Model\Season $season
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Sets the season
     *
     * @param \T3graf\SisBase\Domain\Model\Season $season
     */
    public function setSeason(\T3graf\SisBase\Domain\Model\Season $season)
    {
        $this->season = $season;
    }

    /**
     * Adds a Teams
     *
     * @param \T3graf\SisBase\Domain\Model\Teams $team
     */
    public function addTeam(\T3graf\SisBase\Domain\Model\Teams $team)
    {
        $this->teams->attach($team);
    }

    /**
     * Removes a Teams
     *
     * @param \T3graf\SisBase\Domain\Model\Teams $teamToRemove The Teams to be removed
     */
    public function removeTeam(\T3graf\SisBase\Domain\Model\Teams $teamToRemove)
    {
        $this->teams->detach($teamToRemove);
    }

    /**
     * Returns the teams
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Teams> $teams
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * Sets the teams
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Teams> $teams
     */
    public function setTeams(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $teams)
    {
        $this->teams = $teams;
    }

    /**
     * Adds a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $player
     */
    public function addPlayer(\T3graf\SisBase\Domain\Model\Profiles $player)
    {
        $this->players->attach($player);
    }

    /**
     * Removes a Profiles
     *
     * @param \T3graf\SisBase\Domain\Model\Profiles $playerToRemove The Profiles to be removed
     */
    public function removePlayer(\T3graf\SisBase\Domain\Model\Profiles $playerToRemove)
    {
        $this->players->detach($playerToRemove);
    }

    /**
     * Returns the players
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $players
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Sets the players
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\T3graf\SisBase\Domain\Model\Profiles> $players
     */
    public function setPlayers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $players)
    {
        $this->players = $players;
    }

    /**
     * Returns the competitionMode
     *
     * @return int $competitionMode
     */
    public function getCompetitionMode()
    {
        return $this->competitionMode;
    }

    /**
     * Sets the competitionMode
     *
     * @param int $competitionMode
     */
    public function setCompetitionMode(int $competitionMode)
    {
        $this->competitionMode = $competitionMode;
    }
}
