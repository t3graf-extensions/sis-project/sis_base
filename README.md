# TYPO3 Extension: `SIS - Sport Information System for TYPO3`

[![TYPO3 compatibility](https://img.shields.io/static/v1?label=TYPO3&message=v11&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Packagist PHP Version Support](https://badgen.net/packagist/php/t3graf/sis-base/)](https://packagist.org/packages/t3graf/sis-base)
[![Latest Stable Version](https://badgen.net/packagist/v/t3graf/sis-base/latest?label=stable&color=33a2d8)](https://packagist.org/packages/t3graf/sis-base)
[![Latest Unstable Version](https://badgen.net/packagist/v/t3graf/sis-base/pre?color=orange&label=unstable)](https://packagist.org/packages/t3graf/sis-base)
[![Total Downloads](https://img.shields.io/packagist/dt/t3graf/sis-base?color=1081c1)](https://packagist.org/packages/t3graf/sis-base)
[![License](https://badgen.net/packagist/license/t3graf/sis-base?color=498e7f)](https://gitlab.com/typo3graf/developer-team/extensions/sis_base/-/blob/master/LICENSE.md)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/t3graf-extensions/sis-project/sis_base?branch=master&label=CI%2FCD&logo=gitlab)](https://gitlab.com/t3graf-extensions/sis-project/sis_base/pipelines)
[![coverage report](https://gitlab.com/t3graf-extensions/sis-project/sis_base/badges/master/coverage.svg)](https://gitlab.com/t3graf-extensions/sis-project/sis_base/-/commits/master)

> Sports information system for sports clubs to present team information, player profiles, upcoming matchdays, game results, competition or tournament tables, reports and statistics on the club website.
>
> **Importend!!!**
>
> This extension only provides SIS backend modules and several basic plugin functions (team list view, team detail view, player detail view etc.)
>
> **For full use you have to choose the SIS extension for your sport.**

### Features




## Installation

### Installation using composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org).

`composer require t3graf/sis_base`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.

## Minimal setup

1) Include the static TypoScript of the extension. You don't need to include the TypoScript of Bootstrap package.

<!--## 4. Administration

### Create content element

## 5. Configuration

### Extension settings

### Constants
-->
## Contribute

Please create an issue at [gitlab.com/t3graf-extensions/sis_base/issues](https://gitlab.com/t3graf-extensions/sis-project/sis_base/issues).

**Please use Gitlab only for bug-reports or feature-requests.**

## Credits

This extension was created by Mike Tölle in 2021 for [T3graf media-agentur, Recklinghausen](https://www.t3graf-media.de).

Find examples, use cases and best practices for this extension in our [extended_bootstrap_package blog series on t3graf.de](https://www.t3graf-media.de/blog/).

[Find more TYPO3 extensions we have developed](https://www.t3graf-media.de/) that help us deliver value in client projects. As part of the way we work, we focus on testing and best practices to ensure long-term performance, reliability, and results in all our code.


## Links

- **Demo:** [www.t3graf-media.de/typo3-extensions/sis_base](https://www.t3graf-media.de/typo3-extensions/sis_base)
- **Gitlab Repository:** [gitlab.com/t3graf-extensions/sis_base](https://gitlab.com/t3graf-extensions/sis-project/sis_base)
- **TYPO3 Extension Repository:** [extensions.typo3.org/](https://extensions.typo3.org/)
- **Found an issue?:** [gitlab.com/t3graf-extensions/sis_base/issues](https://gitlab.com/t3graf-extensions/sis-project/sis_base/issues)
