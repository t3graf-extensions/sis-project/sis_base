# 0.0.1

## TASK

- c20b355 [TASK] add documentation settings and rewrite extension description
- f79ad6d [TASK] rename to render-docs
- f726cab [TASK] remove old documentation rendering
- 3460572 [TASK] add documentation rendering
- 1b1de32 [TASK] add XDEBUG_MODE
- bfd6299 [TASK] implement initial unit tests
- 2f4a0ac [TASK] migrate testing
- 9c45e81 [TASK] add coverage in testing
- c6401c5 [TASK] cleanup code
- 287f2e3 [TASK] cleanup code
- 310cf1c [TASK] set php-cs-fixer to typo3 cgl and run cgl
- 1870225 [TASK] fix problems with phpstan
- 3b44367 [TASK] run cgl and fix errors
- 92f943e [TASK] add gitlab-ci
- 3ca0424 [TASK] add main module sis
- fb52792 [TASK] change required php versions
- 742a2de [TASK] change extension icons
- b91cceb [TASK] add README.md and php v8.0 to composer.json

## BUGFIX

- ed8ac44 [BUGFIX] fix problems with shields.io badges
- 2383149 [BUGFIX] fix problem with coverage
- 0222af3 [BUGFIX] fix problem with coverage
- 0fd0cd3 [BUGFIX] fix problem with coverage
- 81340e9 [BUGFIX] fix problem with coverage
- f4b1624 [BUGFIX] add missing FunctionalTests.xml and remove tests for v10.4
- 450818b [BUGFIX] add missing phpstan-baseline.neon

## MISC

- c5d04aa Initial commit

## Contributors

- Mike Tölle

