<?php

/**
 * This file represents the configuration for Code Sniffing PSR-2-related
 * automatic checks of coding guidelines
 * Install @fabpot's great php-cs-fixer tool via
 *
 *  $ composer global require friendsofphp/php-cs-fixer
 *
 * And then simply run
 *
 *  $ php-cs-fixer fix
 *
 * For more information read:
 *  http://www.php-fig.org/psr/psr-2/
 *  http://cs.sensiolabs.org
 */

if (PHP_SAPI !== 'cli') {
    die('This script supports command line usage only. Please check your command.');
}

$header = <<<EOF
This file is part of the package t3graf/sis_base.
For the full copyright and license information, please read the
LICENSE file that was distributed with this source code.
EOF;

$finder = PhpCsFixer\Finder::create()
    ->exclude('.build')
    ->exclude('Contrib')
    ->exclude('var')
    ->exclude('Tests')
    ->in('.build/Web/typo3conf/ext/sis_base/');

return \TYPO3\CodingStandards\CsFixerConfig::create()->setFinder($finder);
