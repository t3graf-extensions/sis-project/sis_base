CREATE TABLE tx_sisbase_domain_model_clubs (
	name varchar(255) NOT NULL DEFAULT '',
	short_name varchar(255) NOT NULL DEFAULT '',
	logo int(11) unsigned NOT NULL DEFAULT '0',
	street varchar(255) NOT NULL DEFAULT '',
	zip varchar(255) NOT NULL DEFAULT '',
	city varchar(255) NOT NULL DEFAULT '',
	country varchar(255) NOT NULL DEFAULT '',
	country_code varchar(255) NOT NULL DEFAULT '',
	lng double(11,2) NOT NULL DEFAULT '0.00',
	lat double(11,2) NOT NULL DEFAULT '0.00',
	short_info text,
	description text,
	founding_year int(11) NOT NULL DEFAULT '0',
	club_colors varchar(255) NOT NULL DEFAULT '',
	www varchar(255) NOT NULL DEFAULT '',
	email varchar(255) NOT NULL DEFAULT '',
	membership int(11) NOT NULL DEFAULT '0',
	directions text,
	locations int(11) unsigned NOT NULL DEFAULT '0'
);

CREATE TABLE tx_sisbase_domain_model_teams (
	name varchar(255) NOT NULL DEFAULT '',
	short_name varchar(255) NOT NULL DEFAULT '',
	logo int(11) unsigned NOT NULL DEFAULT '0',
	images int(11) unsigned NOT NULL DEFAULT '0',
	team_comment text,
	players_comment text,
	coaches_comment text,
	supervisors_comment text,
	club int(11) unsigned DEFAULT '0',
	age_group int(11) unsigned DEFAULT '0',
	coaches text NOT NULL,
	players text NOT NULL,
	supervisors text NOT NULL
);

CREATE TABLE tx_sisbase_domain_model_agegroups (
	name varchar(255) NOT NULL DEFAULT '',
	short_name varchar(255) NOT NULL DEFAULT '',
	logo int(11) unsigned NOT NULL DEFAULT '0'
);

CREATE TABLE tx_sisbase_domain_model_location (
	name varchar(255) NOT NULL DEFAULT '',
	alt_name varchar(255) NOT NULL DEFAULT '',
	capacity int(11) NOT NULL DEFAULT '0',
	logo int(11) unsigned NOT NULL DEFAULT '0',
	images int(11) unsigned NOT NULL DEFAULT '0',
	description text,
	street varchar(255) NOT NULL DEFAULT '',
	city varchar(255) NOT NULL DEFAULT '',
	zip varchar(255) NOT NULL DEFAULT '',
	country varchar(255) NOT NULL DEFAULT '',
	country_code varchar(255) NOT NULL DEFAULT '',
	lng int(11) NOT NULL DEFAULT '0',
	lat int(11) NOT NULL DEFAULT '0'
);

CREATE TABLE tx_sisbase_domain_model_profiles (
	first_name varchar(255) NOT NULL DEFAULT '',
	last_name varchar(255) NOT NULL DEFAULT '',
	stage_name varchar(255) NOT NULL DEFAULT '',
	profile_images int(11) unsigned NOT NULL DEFAULT '0',
	city varchar(255) NOT NULL DEFAULT '',
	birthday date DEFAULT NULL,
	birthplace varchar(255) NOT NULL DEFAULT '',
	nationality varchar(255) NOT NULL DEFAULT '',
	gender int(11) DEFAULT '0' NOT NULL,
	height varchar(255) NOT NULL DEFAULT '',
	weight varchar(255) NOT NULL DEFAULT '',
	contract_start date DEFAULT NULL,
	contract_end date DEFAULT NULL,
	email varchar(255) NOT NULL DEFAULT '',
	nickname varchar(255) NOT NULL DEFAULT '',
	gdpr int(11) DEFAULT '0' NOT NULL,
	images int(11) unsigned NOT NULL DEFAULT '0',
	last_clubs text,
	short_description text,
	description text,
	profile_typ int(11) DEFAULT '0' NOT NULL,
	jersey_number int(11) NOT NULL DEFAULT '0',
	position varchar(255) NOT NULL DEFAULT ''
);

CREATE TABLE tx_sisbase_domain_model_competitions (
	name varchar(255) NOT NULL DEFAULT '',
	short_name varchar(255) NOT NULL DEFAULT '',
	logo int(11) unsigned NOT NULL DEFAULT '0',
	sports int(11) DEFAULT '0' NOT NULL,
	competition_mode int(11) DEFAULT '0' NOT NULL,
	competition_typ int(11) DEFAULT '0' NOT NULL,
	age_groups int(11) unsigned NOT NULL DEFAULT '0',
	season int(11) unsigned DEFAULT '0',
	teams text NOT NULL,
	players text NOT NULL
);

CREATE TABLE tx_sisbase_domain_model_season (
	name varchar(255) NOT NULL DEFAULT '',
	winter_break date DEFAULT NULL
);

CREATE TABLE tx_sisbase_domain_model_matches (
	match_no varchar(255) NOT NULL DEFAULT '',
	round int(11) NOT NULL DEFAULT '0',
	round_name varchar(255) NOT NULL DEFAULT '',
	status int(11) DEFAULT '0' NOT NULL,
	add_info varchar(255) NOT NULL DEFAULT '',
	date datetime DEFAULT NULL,
	visitors int(11) NOT NULL DEFAULT '0',
	match_report text,
	report_author varchar(255) NOT NULL DEFAULT '',
	report_link varchar(255) NOT NULL DEFAULT '',
	images int(11) unsigned NOT NULL DEFAULT '0',
	location int(11) unsigned DEFAULT '0',
	competition int(11) unsigned DEFAULT '0'
);

CREATE TABLE tx_sisbase_domain_model_event (
	name varchar(255) NOT NULL DEFAULT '',
	competitions text NOT NULL
);
