<?php

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

defined('TYPO3') || die();

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'SisBase',
        'sis',
        '',
        'after:web',
        [
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:sis_base/Resources/Public/Icons/modulegroup-sis.svg',
            'labels' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_mod_sis.xlf',

        ]
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'SisBase',
        'sis',
        'teams',
        '',
        [
            \T3graf\SisBase\Controller\TeamsController::class => 'list, new, edit, update, delete',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:sis_base/Resources/Public/Icons/module-teams.svg',
            'labels' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_teams.xlf',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'SisBase',
        'sis',
        'clubs',
        '',
        [
            \T3graf\SisBase\Controller\ClubsController::class => 'list, new, edit, update, delete',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:sis_base/Resources/Public/Icons/module-clubs.svg',
            'labels' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_clubs.xlf',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'SisBase',
        'sis',
        'competitions',
        '',
        [
            \T3graf\SisBase\Controller\CompetitionsController::class => 'list, new, edit, update, delete',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:sis_base/Resources/Public/Icons/module-competitions.svg',
            'labels' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_competitions.xlf',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'SisBase',
        'sis',
        'profiles',
        '',
        [
            \T3graf\SisBase\Controller\ProfilesController::class => 'list, new, edit, update, delete',

        ],
        [
            'access' => 'user,group',
            'icon'   => 'EXT:sis_base/Resources/Public/Icons/module-profiles.svg',
            'labels' => 'LLL:EXT:sis_base/Resources/Private/Language/locallang_profiles.xlf',
        ]
    );

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_clubs', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_clubs.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_clubs');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_teams', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_teams.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_teams');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_agegroups', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_agegroups.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_agegroups');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_location', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_location.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_location');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_profiles', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_profiles.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_profiles');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_competitions', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_competitions.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_competitions');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_season', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_season.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_season');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_matches', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_matches.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_matches');

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_sisbase_domain_model_event', 'EXT:sis_base/Resources/Private/Language/locallang_csh_tx_sisbase_domain_model_event.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_sisbase_domain_model_event');
})();
