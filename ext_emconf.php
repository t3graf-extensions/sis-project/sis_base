<?php

/*
 * This file is part of the package t3graf/sis_base.
 * For the full copyright and license information, please read the
 * LICENSE file that was distributed with this source code.
 */

$EM_CONF[$_EXTKEY] = [
    'title' => 'SIS - Sport Information System for TYPO3',
    'description' => 'Sports information system for sports clubs to present team information, player profiles, upcoming matchdays, game results, competition or tournament tables, reports and statistics on the club website.

Importend!!!
This extension only provides backend modules and basic plugin functions. For full use you have to choose the SIS extension for your sport.',
    'category' => 'module',
    'author' => 'SIS Development Team',
    'author_email' => 'development@t3graf-media.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
            'bootstrap_package' => '*',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
